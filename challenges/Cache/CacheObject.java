package cache;

/**
 * @author Rafael Pacheco
 */
public class CacheObject {
    
    // Attribute examples for CacheObject
    private int IntegerAttribute;
    private String StringAttribute;
    private long LongAttribute;
    private double DoubleAttribute;

    // 

    /**
     * CacheObject constructor for the following (@param)
     * @param IntegerAttribute
     */
    public CacheObject(int IntegerAttribute) {
        this.IntegerAttribute = IntegerAttribute;
    }

    /**
     * CacheObject constructor for the following (@param)
     * @param IntegerAttribute
     * @param StringAttribute
     * @param LongAttribute
     * @param DoubleAttribute
     */
    public CacheObject(int IntegerAttribute, String StringAttribute, long LongAttribute, double DoubleAttribute) {
        this.IntegerAttribute = IntegerAttribute;
        this.StringAttribute = StringAttribute;
        this.LongAttribute = LongAttribute;
        this.DoubleAttribute = DoubleAttribute;
    }

    
    /**
     * Setters and Getters
    */ 
    
    
    /**
     * @return IntegerAttribute
     */
    public int getIntegerAttribute() {
        return IntegerAttribute;
    }

    /**
     * @param IntegerAttribute
     */
    public void setIntegerAttribute(int IntegerAttribute) {
        this.IntegerAttribute = IntegerAttribute;
    }

    /**
     * @return StringAttribute
     */
    public String getStringAttribute() {
        return StringAttribute;
    }

    /**
     * @param StringAttribute
     */
    public void setStringAttribute(String StringAttribute) {
        this.StringAttribute = StringAttribute;
    }

    /**
     * @return LongAttribute
     */
    public long getLongAttribute() {
        return LongAttribute;
    }

    /**
     * @param LongAttribute
     */
    public void setLongAttribute(long LongAttribute) {
        this.LongAttribute = LongAttribute;
    }

    /**
     * @return DoubleAttribute
     */
    public double getDoubleAttribute() {
        return DoubleAttribute;
    }

    /**
     * @param DoubleAttribute
     */
    public void setDoubleAttribute(double DoubleAttribute) {
        this.DoubleAttribute = DoubleAttribute;
    }

    @Override
    public String toString() {
        return ""+IntegerAttribute;
    }

}
