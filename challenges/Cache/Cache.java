package cache;

import java.util.TreeMap;

/**
 * @author Rafael Pacheco
 */
public class Cache {

    // TreeMap object in order to obtain the lowest key value which means the oldest cache object
    // Parameters - Long (to store time of creation) - CacheObject (to store cache information)
    TreeMap<Long, CacheObject> tmap = new TreeMap<>();
    private int max;

    /**
     *
     * @param max - max number of CacheObject
     */
    public Cache(int max) {
        tmap = new TreeMap<>();
        this.max = max;
    }

    /**
     * @param Integer - a simple example for setting CacheObject attribute
     * (IntegerAttribute)
     */
    public void add(int Integer) {
        if (tmap.size() < this.max) {
            tmap.put(System.currentTimeMillis(), new CacheObject(Integer));
        } else {
            tmap.remove(tmap.firstKey());
            tmap.put(System.currentTimeMillis(), new CacheObject(Integer));
        }
        System.out.println(" > added: {" + Integer + "}");
    }

    /**
     * Print the current CacheObject attributes on the TreeMap
     */
    public void print() {
        System.out.println(tmap.values().toString());
        System.out.println();
    }

    /**
     * Update the oldest CacheObject attribute (IntegerAttribute)
     */
    public void UpdateCacheData(Long keyValue, int Integer) {
        tmap.replace(keyValue, new CacheObject(Integer));
    }

    /**
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        Cache cache = new Cache(3);

        cache.add(1); // create a new CacheObject and set its IntegerAttribute as 1
        cache.print(); // print the current CacheObject attributes stored on the TreeMap
        Thread.sleep(1000); // sleep in order to create time differences between CacheObject timestamp creations

        cache.add(2);
        cache.print();
        Thread.sleep(1000);

        cache.add(10);
        cache.print();
        Thread.sleep(1000);

        cache.add(5);
        cache.print();
        Thread.sleep(1000);

        cache.add(2);
        cache.print();
        Thread.sleep(1000);

        cache.add(10);
        cache.print();
        Thread.sleep(1000);

        cache.add(5);
        cache.print();
        Thread.sleep(1000);

        cache.add(15);
        cache.print();

        // Update the oldest CacheObject on the TreeMap
        System.out.println("> Updated the oldest CacheObject with value: " + cache.tmap.get(cache.tmap.firstKey()) + " to 99");
        cache.UpdateCacheData(cache.tmap.firstKey(), 99);
        cache.print();

        // Update the newest CacheObject on the TreeMap
        System.out.println("> Updated the newest CacheObject with value: " + cache.tmap.get(cache.tmap.lastKey()) + " to 55");
        cache.UpdateCacheData(cache.tmap.lastKey(), 55);
        cache.print();
    }

}
