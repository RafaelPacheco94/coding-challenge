package udp_sv_client;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael Pacheco
 */
public class udp_cli {

    public static void main(String[] args) {
        udp_cli cli = new udp_cli();
        /**
         * By using args as input
         */
        //        String ip_s = args[0];
        //        String time_s = args[2];
        //        String bandwidth_s = args[4];
        //
        //        String[] split = ip_s.split(".");
        //        int[] ip_i = new int[]{Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3])};
        //        byte[] ip = new byte[]{(byte) ip_i[0], (byte) ip_i[1], (byte) ip_i[2], (byte) ip_i[3]};
        //
        //        int time = Integer.parseInt(time_s);
        //        double bandwidth = Double.parseDouble(bandwidth_s);
        //        try {
        //            InetAddress server_address = InetAddress.getByAddress(ip);
        //
        //            cli.udp_cli_run(server_address, time, bandwidth);
        //        } catch (UnknownHostException ex) {
        //            Logger.getLogger(udp_cli.class.getName()).log(Level.SEVERE, null, ex);
        //        }

        /**
         * By using static inputs
         */
        try {
            cli.udp_cli_run(InetAddress.getByAddress(new byte[]{(byte) 192, (byte) 168, (byte) 0, (byte) 104}), 5, 150);
        } catch (UnknownHostException ex) {
            Logger.getLogger(udp_cli.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private DatagramSocket Socket;
    private DatagramPacket Packet;
    private final int server_port = 1234; // Port for Server
    private int client_port = 4321; // Port for Client
    private InetAddress server_address;
    private final int udp_packet_size = 1000; // udp packet size
    private byte[] RX_buffer; // buffer for reception
    private byte[] TX_buffer; // buffer for transmission
    private int NumberOfReceivedBytes = 0; // number of total received bytes
    private int NumberOfReceivedBytesOverOneSecond = 0; // number o received bytes at each second
    private double AverageBandwidth = 0; // Average bandwidth
    private int TransmissionTime; // Transmission time
    private double BW;
    private static DecimalFormat df1 = new DecimalFormat("#.##");
    private static DecimalFormat df2 = new DecimalFormat("#.####");
    private double latency_ms; // latency in milliseconds
    private double latency_us; // latency in microseconds
    java.util.concurrent.atomic.AtomicInteger TimeCounter; // counter for each elapsed second
    Timer t = new Timer(true);
    TimerTask task;

    public udp_cli() {
        task = new TimerTask() {
            @Override
            public void run() {
                TimeCounter.incrementAndGet();

                if (TimeCounter.get() <= TransmissionTime) {
                    NumberOfReceivedBytes += NumberOfReceivedBytesOverOneSecond;
                    PrintData();
                }
            }
        };
    }

    /**
     * @param args the command line arguments
     */
    public void udp_cli_run(InetAddress sv_add, int time, double bandwidth) {

        TimeCounter = new AtomicInteger(0);
        TransmissionTime = time;
        BW = bandwidth;
        CreateSocketConnectToServer(sv_add);
        if (Socket.isConnected()) {
            SendParameters(time, bandwidth);
            if (ReceiveResponse()) {
                ReceivePacketsFromServer();
                TradePacketsWithServer();
                PrintFinalResults();

            } else {
                System.out.println("Invalid Response from server!");
            }
        } else {
            System.out.println("Server may be offline or unavailable!");
        }
        Socket.close();

    }

    private void SendParameters(int time, double bandwidth) {
        try {
            Socket.setSoTimeout(2000);
            String message = "-t " + time + " -r " + bandwidth;
            TX_buffer = new byte[message.getBytes().length];
            System.arraycopy(message.getBytes(), 0, TX_buffer, 0, message.getBytes().length);
            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, server_address, server_port);
            Socket.send(Packet);
        } catch (SocketException ex) {
            Logger.getLogger(udp_cli.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(udp_cli.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private boolean ReceiveResponse() {

        try {
            RX_buffer = new byte[udp_packet_size];
            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            String msg = new String(Packet.getData(), 0, Packet.getLength());
            if (msg.equals("Ok")) {
                return true;
            } else {
                return false;
            }
        } catch (IOException ex) {
            return false;
        }

    }

    private void ReceivePacketsFromServer() {

        try {
            RX_buffer = new byte[udp_packet_size];
            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            NumberOfReceivedBytesOverOneSecond += Packet.getLength();
            t.scheduleAtFixedRate(task, 900, 1000);
            while (TimeCounter.get() < TransmissionTime) {
                Socket.receive(Packet);
                if (Packet.getLength() == udp_packet_size && Packet.getAddress().equals(server_address)) {
                    NumberOfReceivedBytesOverOneSecond += Packet.getLength();
                }
            }
            task.cancel();
        } catch (IOException ex) {

        }
    }

    private void PrintData() {
        double bandwidth = NumberOfReceivedBytesOverOneSecond * 8;
        bandwidth = (double) (bandwidth * Math.pow(10, -6));
        BigDecimal bw = BigDecimal.valueOf(bandwidth).setScale(2, RoundingMode.CEILING);
        bandwidth = bw.doubleValue();
        AverageBandwidth += bandwidth;
        System.out.println("> " + ((float) TimeCounter.get() - 1) + " - " + ((float) TimeCounter.get()) + " sec received " + NumberOfReceivedBytesOverOneSecond + " bytes at " + bandwidth + " Mbits/s");
        NumberOfReceivedBytesOverOneSecond = 0;
    }

    private void CreateSocketConnectToServer(InetAddress sv_add) {
        try {
            Socket = new DatagramSocket(client_port);
            Socket.connect(sv_add, server_port);
            server_address = sv_add;
        } catch (SocketException ex) {
            Logger.getLogger(udp_cli.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void PrintFinalResults() {
        double bw = AverageBandwidth / TransmissionTime;
        double NumberOfTheoreticalReceivedBytes = (double) ((BW * Math.pow(10, 6)) / 8) * TransmissionTime;
        double packetloss = 100 - (((double) NumberOfReceivedBytes / NumberOfTheoreticalReceivedBytes) * 100);
        System.out.println();

        System.out.println("> 0.0 - " + ((float) TransmissionTime) + " sec received " + NumberOfReceivedBytes + " bytes out of " + (long) NumberOfTheoreticalReceivedBytes + " at " + df1.format(bw) + " Mbits/s, with " + df2.format(packetloss) + "% packet loss and a latency (based on the exchange of 4 packets) of " + latency_ms + " ms or " + latency_us + " us");

    }

    private void TradePacketsWithServer() {
        try {
            String time_size = Long.toString(System.nanoTime());
            Socket.setSoTimeout(3000);
            RX_buffer = new byte[(2 * time_size.getBytes().length)];
            TX_buffer = new byte[(2 * time_size.getBytes().length)];
            Arrays.fill(RX_buffer, (byte) 0);

            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, server_address, server_port);
            Socket.send(Packet);
            long TX_time_0 = System.nanoTime();
            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            long RX_time_0 = System.nanoTime();
            byte[] sv_RX_time_0 = new byte[time_size.getBytes().length];
            byte[] sv_TX_time_0 = new byte[time_size.getBytes().length];
            System.arraycopy(RX_buffer, 0, sv_RX_time_0, 0, time_size.getBytes().length);
            System.arraycopy(RX_buffer, time_size.getBytes().length, sv_TX_time_0, 0, time_size.getBytes().length);
            long sv_rx_time0 = Long.parseLong(new String(sv_RX_time_0));
            long sv_tx_time0 = Long.parseLong(new String(sv_TX_time_0));

            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, server_address, server_port);
            Socket.send(Packet);
            long TX_time_1 = System.nanoTime();
            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            long RX_time_1 = System.nanoTime();
            byte[] sv_RX_time_1 = new byte[time_size.getBytes().length];
            byte[] sv_TX_time_1 = new byte[time_size.getBytes().length];
            System.arraycopy(RX_buffer, 0, sv_RX_time_1, 0, time_size.getBytes().length);
            System.arraycopy(RX_buffer, time_size.getBytes().length, sv_TX_time_1, 0, time_size.getBytes().length);
            long sv_rx_time1 = Long.parseLong(new String(sv_RX_time_1));
            long sv_tx_time1 = Long.parseLong(new String(sv_TX_time_1));

            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, server_address, server_port);
            Socket.send(Packet);
            long TX_time_2 = System.nanoTime();
            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            long RX_time_2 = System.nanoTime();
            byte[] sv_RX_time_2 = new byte[time_size.getBytes().length];
            byte[] sv_TX_time_2 = new byte[time_size.getBytes().length];
            System.arraycopy(RX_buffer, 0, sv_RX_time_2, 0, time_size.getBytes().length);
            System.arraycopy(RX_buffer, time_size.getBytes().length, sv_TX_time_2, 0, time_size.getBytes().length);
            long sv_rx_time2 = Long.parseLong(new String(sv_RX_time_2));
            long sv_tx_time2 = Long.parseLong(new String(sv_TX_time_2));

            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, server_address, server_port);
            Socket.send(Packet);
            long TX_time_3 = System.nanoTime();
            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            long RX_time_3 = System.nanoTime();
            byte[] sv_RX_time_3 = new byte[time_size.getBytes().length];
            byte[] sv_TX_time_3 = new byte[time_size.getBytes().length];
            System.arraycopy(RX_buffer, 0, sv_RX_time_3, 0, time_size.getBytes().length);
            System.arraycopy(RX_buffer, time_size.getBytes().length, sv_TX_time_3, 0, time_size.getBytes().length);
            long sv_rx_time3 = Long.parseLong(new String(sv_RX_time_3));
            long sv_tx_time3 = Long.parseLong(new String(sv_TX_time_3));

            long res0 = RX_time_0 - TX_time_0 - (sv_tx_time0 - sv_rx_time0);
            long res1 = RX_time_1 - TX_time_1 - (sv_tx_time1 - sv_rx_time1);
            long res2 = RX_time_2 - TX_time_2 - (sv_tx_time2 - sv_rx_time2);
            long res3 = RX_time_3 - TX_time_3 - (sv_tx_time3 - sv_rx_time3);
            latency_us = TimeUnit.NANOSECONDS.toMicros((res0 + res1 + res2 + res3) / 4);
            latency_ms = TimeUnit.NANOSECONDS.toMillis((res0 + res1 + res2 + res3) / 4);

        } catch (SocketException ex) {
            Logger.getLogger(udp_cli.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(udp_cli.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
