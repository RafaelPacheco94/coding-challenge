package udp_sv_client;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.lang.Math.round;
import java.text.DecimalFormat;

/**
 * @author Rafael Pacheco
 *
 */
public class udp_sv {

    public static void main(String[] args) {
        udp_sv sv = new udp_sv();
        sv.udp_sv_run();
    }
    
    private DatagramSocket Socket;
    private DatagramPacket Packet;
    private final int server_port = 1234; // Port for Server
    private int client_port; // Port for Clients
    private final int udp_packet_size = 1000; // udp packet size
    private byte[] RX_buffer; // buffer for reception
    private byte[] TX_buffer; // buffer for transmission
    private InetAddress client_address; // client address
    private int TransmissionTime; // time of transmission specified by the client
    private float Bandwidth_Mbytes; // bandwidth in Mbytes according to the specified by the client
    private int AmountOfPackets; // amount of packets to transmit each second
    private int NumberOfTransmitedBytesOverOneSecond = 0; // number o transmitted bytes at each second
    private int NumberOfTransmitedBytes = 0; // number of total transmitted bytes
    private double AverageBandwidth; // Average bandwidth
    java.util.concurrent.atomic.AtomicInteger TimeCounter; // counter for each elapsed second
    Timer t = new Timer(true);
    private static DecimalFormat df1 = new DecimalFormat("#.##");

    public void udp_sv_run() {
        TimeCounter = new AtomicInteger(0);
        while (true) {
            String ClientParameters = ReceiveParameters(); // Get client parameters
            if (GetParameters(ClientParameters) && SendOk()) {
                CalculateTransmissionInterval();
                SendPacketsToClient();
                TradePacketsWithClient();
                System.out.println();
                Socket.close();
            }
        }
    }

    private String ReceiveParameters() {
        try {
            Socket = new DatagramSocket(server_port);
            Socket.setSoTimeout(0);
            RX_buffer = new byte[udp_packet_size];
            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            System.out.println("Waiting for a client...");
            Socket.receive(Packet);
            System.out.println("New Client Connected");
            client_port = Packet.getPort();
            client_address = Packet.getAddress();
            String rcv = new String(Packet.getData(), 0, Packet.getLength());
            return rcv;
        } catch (SocketException ex) {
            return "";
        } catch (IOException ex) {
            return "";
        }
    }

    private boolean GetParameters(String ClientParameters) {
        String[] rcv_splited = ClientParameters.split(" ");
        if ((rcv_splited[0].equals("-t") && rcv_splited[2].equals("-r")) && (IsInteger(rcv_splited[1]) && IsFloat(rcv_splited[3]))) {
            TransmissionTime = Integer.parseInt(rcv_splited[1]);
            float bw_Mbits = Float.parseFloat(rcv_splited[3]);
            bw_Mbits = (float) (bw_Mbits * Math.pow(10, 6));
            Bandwidth_Mbytes = bw_Mbits / 8;
            return true;
        } else {
            return false;
        }
    }

    private boolean IsInteger(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean IsFloat(String s) {
        try {
            Float.parseFloat(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean SendOk() {
        try {
            String msg = "Ok";
            TX_buffer = new byte[msg.getBytes().length];
            System.arraycopy(msg.getBytes(), 0, TX_buffer, 0, msg.getBytes().length);
            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, client_address, client_port);
            Socket.send(Packet);
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    private void CalculateTransmissionInterval() {
        BigDecimal udp_w_size = new BigDecimal(udp_packet_size);
        BigDecimal udp_bw_Mbytes = new BigDecimal(Bandwidth_Mbytes);
        BigDecimal ts = udp_w_size.divide(udp_bw_Mbytes, 15, RoundingMode.HALF_UP);
        BigDecimal ts1 = udp_bw_Mbytes.divide(udp_w_size, 15, RoundingMode.HALF_UP);
        AmountOfPackets = (int) round(ts1.doubleValue());
    }

    private void PrintData() {
        double bandwidth = NumberOfTransmitedBytesOverOneSecond * 8;
        bandwidth = (double) (bandwidth * Math.pow(10, -6));
        BigDecimal bw = BigDecimal.valueOf(bandwidth).setScale(2, RoundingMode.CEILING);
        bandwidth = bw.doubleValue();
        AverageBandwidth += bandwidth;
        System.out.println("> " + ((float) TimeCounter.get() - 1) + " - " + ((float) TimeCounter.get()) + " sec sent " + NumberOfTransmitedBytesOverOneSecond + " bytes at " + bandwidth + " Mbits/s");
        NumberOfTransmitedBytesOverOneSecond = 0;

    }

    private void Transmit() {
        try {
            for (int i = 0; i < AmountOfPackets; i++) {
                Socket.send(Packet);
                NumberOfTransmitedBytesOverOneSecond += Packet.getLength();
            }
        } catch (IOException ex) {
            Logger.getLogger(udp_sv.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void PrintFinalResults() {
        double bw = AverageBandwidth / TransmissionTime;
        System.out.println("> finished");
        System.out.println("> 0.0 - " + ((float) TransmissionTime) + " sec sent " + NumberOfTransmitedBytes + " bytes at " + df1.format(bw) + " Mbits/s");
        NumberOfTransmitedBytes = 0;
        AverageBandwidth = 0;
    }

    @SuppressWarnings("empty-statement")
    private void SendPacketsToClient() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(udp_sv.class.getName()).log(Level.SEVERE, null, ex);
        }
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                TimeCounter.incrementAndGet();
                if (TimeCounter.get() <= TransmissionTime) {
                    Transmit();
                    NumberOfTransmitedBytes += NumberOfTransmitedBytesOverOneSecond;
                    PrintData();
                }
            }
        };
        TX_buffer = new byte[udp_packet_size];
        new Random().nextBytes(TX_buffer);
        Packet = new DatagramPacket(TX_buffer, TX_buffer.length, client_address, client_port);

        t.scheduleAtFixedRate(task, 0, 1000);
        while (TimeCounter.get() != TransmissionTime + 1);
        task.cancel();
        TimeCounter.set(0);
        PrintFinalResults();
    }

    private void TradePacketsWithClient() {
        try {
            String time_size = Long.toString(System.nanoTime());
            Socket.setSoTimeout(3000);

            RX_buffer = new byte[(2 * time_size.getBytes().length)];
            TX_buffer = new byte[(2 * time_size.getBytes().length)];

            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            String RX_time_0 = Long.toString(System.nanoTime());
            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, client_address, client_port);
            System.arraycopy(RX_time_0.getBytes(), 0, TX_buffer, 0, RX_time_0.getBytes().length);
            String TX_time_0 = Long.toString(System.nanoTime());
            System.arraycopy(TX_time_0.getBytes(), 0, TX_buffer, RX_time_0.getBytes().length, TX_time_0.getBytes().length);
            Socket.send(Packet);

            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            String RX_time_1 = Long.toString(System.nanoTime());
            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, client_address, client_port);
            System.arraycopy(RX_time_1.getBytes(), 0, TX_buffer, 0, RX_time_1.getBytes().length);
            String TX_time_1 = Long.toString(System.nanoTime());
            System.arraycopy(TX_time_1.getBytes(), 0, TX_buffer, RX_time_1.getBytes().length, TX_time_1.getBytes().length);
            Socket.send(Packet);

            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            String RX_time_2 = Long.toString(System.nanoTime());
            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, client_address, client_port);
            System.arraycopy(RX_time_2.getBytes(), 0, TX_buffer, 0, RX_time_2.getBytes().length);
            String TX_time_2 = Long.toString(System.nanoTime());
            System.arraycopy(TX_time_2.getBytes(), 0, TX_buffer, RX_time_2.getBytes().length, TX_time_2.getBytes().length);
            Socket.send(Packet);

            Packet = new DatagramPacket(RX_buffer, RX_buffer.length);
            Socket.receive(Packet);
            String RX_time_3 = Long.toString(System.nanoTime());
            Packet = new DatagramPacket(TX_buffer, TX_buffer.length, client_address, client_port);
            System.arraycopy(RX_time_3.getBytes(), 0, TX_buffer, 0, RX_time_3.getBytes().length);
            String TX_time_3 = Long.toString(System.nanoTime());
            System.arraycopy(TX_time_3.getBytes(), 0, TX_buffer, RX_time_3.getBytes().length, TX_time_3.getBytes().length);
            Socket.send(Packet);

        } catch (SocketException ex) {

        } catch (IOException ex) {

        }

    }

}
